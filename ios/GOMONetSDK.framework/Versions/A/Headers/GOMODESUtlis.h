//
// Created by zhangjiemin on 2018/6/14.
// Copyright (c) 2018 zhangjiemin000. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GOMODESUtlis : NSObject


+ (NSData *)encryptUseDES2:(NSString *)plainText key:(NSString *)key keyBase64:(BOOL)keyNeedBase64;

+ (NSString *)decryptUseDESString:(NSString *)cipherText key:(NSString *)key;

+ (NSString *)decryptUseDES:(NSData *)cipherData key:(NSString *)key keyNeedBase64:(BOOL)keyNeedBase64;

+ (NSString *)encryptWithText:(NSString *)sText forKey:(NSString *)key;

+ (NSString *)decryptWithText:(NSData *)sText forKey:(NSString *)key;
@end